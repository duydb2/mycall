#!/bin/bash

# example
# https://github.com/sqwiggle/webrtc-ios/blob/master/build_webrtc
# https://github.com/pristineio/webrtc-build-scripts/blob/master/android/build.sh

SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SOURCE="$(readlink "$SOURCE")"
[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

PROJECT_ROOT="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
WORK_DIR=`pwd`

## get deopt_tools
[ -d ${WORK_DIR}/tools ] || (mkdir -p tools && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git ${WORK_DIR}/tools/depot_tools)
export PATH=${WORK_DIR}/tools/depot_tools:$PATH
gclient

## get source
[ -d ${WORK_DIR}/thirdparty/google ] || (mkdir -p ${WORK_DIR}/thirdparty/google && ([ -d ${WORK_DIR}/thirdparty/google/src ] || (cd ${WORK_DIR}/thirdparty/google && fetch webrtc)))
echo "target_os = ['ios', 'mac']" >> .gclient
#gclient sync

# build webrtc
build_android(){
    src_dir=${WORK_DIR}/thirdparty/google
    out_dir=${WORK_DIR}/thirdparty/google/src/out_android
    export GYP_DEFINES="OS=android host_os=linux libjingle_java=1 build_with_libjingle=1 build_with_chromium=0 enable_tracing=1 enable_android_opensl=0"
    export GYP_GENERATORS="ninja"
    export GYP_DEFINES="$GYP_DEFINES OS=android"
    export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=out_android_armeabi-v7a"
    export GYP_CROSSCOMPILE=1
    echo "ARMv7 with Neon Build"

    pushd ${src_dir}
    gclient runhooks --force
    ninja -C ${out_dir}/Debug
    popd
}

build_ios(){
    src_dir=${WORK_DIR}/thirdparty/google
    out_dir=${WORK_DIR}/thirdparty/google/src/out_ios
    export GYP_DEFINES="build_with_libjinglth_chromium=0 libjingle_objc=1"
    export GYP_GENERATORS="ninja"
    export GYP_DEFINES="$GYP_DEFINES OS=ios target_arch=armv7s"
    export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=${out_dir}"
    export GYP_CROSSCOMPILE=1

    pushd ${src_dir}
    gclient run-hooks --force
    ninja -C ${out_dir}/Debug
    popd
}

build_mac(){
    src_dir=${WORK_DIR}/thirdparty/google
    out_dir=${WORK_DIR}/thirdparty/google/src/out_mac
    export GYP_DEFINES="build_with_libjinglth_chromium=0 libjingle_objc=1"
    export GYP_GENERATORS="ninja"
    export GYP_DEFINES="$GYP_DEFINES OS=mac target_arch=x64"
    export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=${out_dir}"

    pushd ${src_dir}
    gclient run-hooks --force
    ninja -C ${out_dir}/Debug
    popd
}

pushd ${WORK_DIR}/thirdparty/google
gclient run-hooks --force
ninja -C src/out/Debug
popd